﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class muelle : MonoBehaviour {

    private float initialPosition;
    private float finalPosition;
    private bool pressed;
    private bool shoot;
    public float contador;
    private GameObject bola;
    private float force;

	// Use this for initialization
	void Start () {
        initialPosition = 0.40f;
        finalPosition = 0.47f;
        pressed = false;
        shoot = false;
        contador = 0.6f;
        bola = GameObject.FindGameObjectWithTag("ball");
        force = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space)){
            pressed = true;
        }
        else if(Input.GetKeyUp(KeyCode.Space)){
            pressed = false;
            shoot = true;
        }
        
        if(pressed && !shoot){
            Debug.Log(transform.position.y);
            if(contador > 0.001f && transform.position.y > 29.9f) {
                transform.position = new Vector3(transform.position.x, transform.position.y - 2, transform.position.z);
                contador = 0;
            }
            else {
                contador += Time.deltaTime;
            }
            
        }

        if (shoot){
            Debug.Log(force);
            if (contador > 0.001f && transform.position.y < 47.4f) {
                transform.position = new Vector3(transform.position.x, transform.position.y + 5, transform.position.z);
                contador = 0;
                force += 400;
            }
            else if (transform.position.y < 47.4f) { 
                contador += Time.deltaTime;
            }
            else {
                bola.GetComponent<Rigidbody>().AddForce(transform.position.x, transform.position.y + force, transform.position.z);
                shoot = false;
                force = 0;
            }
        }
	}
}
